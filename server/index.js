import 'babel-polyfill'

import Koa from 'koa'
import cors from 'kcors'
import { Exceptions, ExceptionHandler } from './exceptions'
import router from './server/routes/index'
import io from './server/wss/wss'

const app = new Koa();
/**
  Middlewares
**/

app.use(cors());

app
  // Counting time
  .use(async (ctx, next) => {
    let start = Date.now();
    await next();
    const now = new Date().toISOString()
      .replace(/T/, ' ')
      .replace(/\..+/, '');
    console.log(`[${now}][${ctx.request.method}][${ctx.request.url}] ${Date.now() - start} ms.`);
  })
  .use(async (ctx, next) => {
    try {
      await next();
      if (!ctx.body)
        throw new Exceptions.NotFound(`Endpoint [${ctx.request.url}] not found.`);
      ctx.body = {
        ok: true,
        content: ctx.body
      };
    } catch (e) {
      ctx.body = ExceptionHandler(e);
    }
  })
  .use(async (ctx, next) => {
    ctx.state = {};
    ctx.state.query = ctx.request.query;
    ctx.state.body = ctx.request.body;
    await next();
  })
  // routes
  .use(router.routes())
  // Allowed methods
  .use(router.allowedMethods());

/**
 launch
**/
io.attach(app);
app.listen(3000, () => {
  console.log('Listening on port 3000');
});
