import Router from 'koa-router'
import main from './main/main'

const router = new Router();

router.use('/', main.routes(), main.allowedMethods());

export default router;
