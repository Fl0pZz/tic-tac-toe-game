
export default class Field {
  static key (x, y) { return `${x}|${y}` }
  constructor () {
    this._field = new Map()
  }
  set({ x, y, id }) {
    this._field.set(Field.key(x, y), { owner: id })
  }
  isWin ({ uid, x, y }) {
    const hasFiveInRow = (deltaX, deltaY) => {
      let count = 0;
      for (let i = x - 4 * deltaX, j = y - 4 * deltaY; (i > x - 5 && i < x + 5) && (j > y - 5 && j < y + 5); i += deltaX, j += deltaY) {
        if (this._field.has(Field.key(i, j)) && this._field.get(Field.key(i, j)).owner === uid) {
          if (++count === 5) { return true }
        } else { count = 0 }
      }
      return false
    };
    return [
      hasFiveInRow(1, -1),
      hasFiveInRow(1, 0),
      hasFiveInRow(1, 1),
      hasFiveInRow(0, 1)
    ].some(Boolean)
  }

  createField10 (offsetX, offsetY) {
    const out = [];
    for (let y = 0; y < 10; ++y) {
      out[y] = [];
      for (let x = 0; x < 10; ++x) {
        if (this._field.has(Field.key(offsetX + x, offsetY + y))) {
          out[y][x] = this._field.get(Field.key(offsetX + x, offsetY + y))
        } else {
          out[y][x] = { owner: null };
        }
      }
    }
    return out;
  }
}
