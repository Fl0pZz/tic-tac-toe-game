export default class UserManager {
  constructor () {
    this.users = {};
    this._stp = this._step();
    this._current = null
  }
  add (uid) {
    this.users[uid] = {
      offsetX: 0,
      offsetY: 0
    };
  }
  coords (x, y, uid) {
    const { offsetX, offsetY } = this.users[uid];
    return { x: offsetX + x, y: offsetY + y, uid }
  }
  has (uid) {
    return this.users[uid] !== undefined
  }
  del (uid) {
    delete this.users[uid];
    if (this._current === uid) {
      this._current = Object.keys(this.users).length
        ? this.step()
        : null
    }
  }
  *_step (isEnd = false) {
    while (true) {
      for (let uid in this.users) {
        if (isEnd) { return; }
        yield uid;
      }
    }
  }
  step () {
    this._current = this._stp.next().value;
    return this._current
  }
  whoseTurn () {
    return this._current
  }
}
