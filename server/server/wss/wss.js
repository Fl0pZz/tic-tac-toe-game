import IO from 'koa-socket'
import * as seTypes from './socket-event-types'
import Field from '../field/field'
import UserManager from '../users/users'

const io = new IO();

const field = new Field();
const userManager = new UserManager();

io.use( async (ctx, next) => { // logger
  let start = Date.now();
  await next();
  const now = new Date().toISOString()
    .replace(/T/, ' ')
    .replace(/\..+/, '');
  console.log(`[${now}][wss] ${Date.now() - start} ms.`);
});

io.on(seTypes.CONNECTION, (ctx, data) => {
  // console.log(seTypes.CONNECTION, ctx.socket.client.id, userManager.users);

  let id = ctx.socket.client.id;
  userManager.add(id);
  io.broadcast(seTypes.NEW_PLAYER, id);
  io.broadcast(seTypes.GET_FIELD, { id, field: field.createField10(0, 0) });
  if (Object.keys(userManager.users).length >= 2 && !userManager._current) {
    io.broadcast(seTypes.NEXT_STEP, userManager.step());
    console.log('let start', id, userManager._current);
  }
  console.log(seTypes.CONNECTION, ctx.socket.client.id, userManager);
});

io.on(seTypes.PLAYER_LEFT, (ctx, data) => {
  console.log(seTypes.PLAYER_LEFT, ctx.socket.socket.client.id, userManager.whoseTurn());

  const id = ctx.socket.socket.client.id;
  const delId = userManager.whoseTurn();
  userManager.del(id);
  io.broadcast(seTypes.NEXT_STEP, userManager.whoseTurn());
  io.broadcast(seTypes.PLAYER_LEFT, id);
  console.log(seTypes.PLAYER_LEFT, ctx.socket.socket.client.id, userManager.whoseTurn());
});

io.on(seTypes.GET_FIELD, (ctx, { x, y }) => {
  console.log(seTypes.GET_FIELD, ctx.socket.socket.client.id, `x: ${x}`, `y: ${y}`);
  const id = ctx.socket.socket.client.id;
  io.broadcast(seTypes.GET_FIELD, { id, field: field.createField10(x, y)});
});

io.on(seTypes.UPDATE, (ctx, { x, y }) => {
  console.log(seTypes.UPDATE, ctx.socket.socket.client.id, `x: ${x}`, `y: ${y}`, Object.keys(userManager.users));
  let id = ctx.socket.socket.client.id;
  if (!userManager.has(id) && userManager.whoseTurn() !== id) return;
  field.set({ ...userManager.coords(x, y, id), id });
  //console.log(1, { ...userManager.coords(x, y, id), id }, field._field);
  console.log(1, field.isWin(userManager.coords(x, y, id)), { x, y, id });
  if (field.isWin(userManager.coords(x, y, id))) {
    io.broadcast(seTypes.GAME_OVER, id);
  } else {
    io.broadcast(seTypes.UPDATE, { x, y, id });
    id = userManager.step();
    io.broadcast(seTypes.NEXT_STEP, id)
  }
});

export default io;
