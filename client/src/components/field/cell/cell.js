import { mapState } from 'vuex'
import * as mTypes from '../../../store/mutations-types'
import store from '../../../store/store'

export default {
  name: 'cell',
  props: [ 'coords'],
  // watch: {
  //   owner (newVal) {
  //     console.log('watcher', newVal)
  //   }
  // },
  computed: {
    ...mapState({
      owner (state) {
        return state.field.field[this.coords.y][this.coords.x].owner
      },
      canAct: (state) => state.user.player.canAct
    })
  },
  methods: {
    markPoint () {
      console.log('markPoint', this.coords.x, this.coords.y, this.coords);
      if (!this.canAct) { return }

      store.dispatch('update', this.coords);
    },
  }
}
