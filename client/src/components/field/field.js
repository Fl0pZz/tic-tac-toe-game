import Cell from './cell/Cell.vue'
import * as mTypes from '../../store/mutations-types'
import * as seTypes from '../../socket-event-types'
import { mapState } from 'vuex'
import store from '../../store/store'
import socket from '../../ws'

export default {
  name: 'field',
  components: { Cell },
  created () {
    window.onbeforeunload = () => {
      console.log(seTypes.PLAYER_LEFT, this.$socket.id);
      socket.emit(seTypes.PLAYER_LEFT);
    };
    const $this = this;
    window.addEventListener('keyup', function(e) {
      switch (e.keyCode) {
        case 38:
          $this.goUp();
          break;
        case 40:
          $this.goDown();
          break;
        case 37:
          $this.goLeft();
          break;
        case 39:
          $this.goRight();
          break;
      }
    })
  },
  sockets:{
    [seTypes.CONNECTION]: function () {
      console.log(seTypes.CONNECTION, this.$socket.id);
      const id = this.$socket.id;
      store.commit(mTypes.CONNECT, id);
    },
    [seTypes.NEW_PLAYER]: function (id) {
      console.log(seTypes.NEW_PLAYER, id);
      store.commit(mTypes.SOCKET_NEW_PLAYER, id);
    },
    [seTypes.PLAYER_LEFT]: function (id) {
      console.log(seTypes.PLAYER_LEFT, id);
    },
    [seTypes.NEXT_STEP]: function (id) {
      console.log(seTypes.NEXT_STEP, id, Object.keys(store.state.user.players.uids), !Object.keys(store.state.user.players.uids).some(uid => uid === id));
      store.commit(mTypes.SOCKET_NEXT_STEP, id);
      if (!Object.keys(store.state.user.players.uids).some(uid => uid === id)) {
        store.commit(mTypes.SOCKET_NEW_PLAYER, id);
      }
    },
    [seTypes.GET_FIELD]: function ({ id, field }) {
      console.log(seTypes.GET_FIELD, field);
      if (this.id !== id) { return }

      for (let y = 0; y < 10; ++y) {
        for (let x = 0; x < 10; ++x) {
          if (!field[y][x].owner) { continue }
          if (!store.state.user.players.uids[field[y][x].owner]) {
            store.commit(mTypes.SOCKET_NEW_PLAYER, field[y][x].owner);
            console.log('get field', field[y][x].owner, store.state.user.players.uids[field[y][x].owner]);
          }
          field[y][x].owner = store.state.user.players.uids[field[y][x].owner]
        }
      }
      store.commit(mTypes.SOCKET_GET_FIELD, field);
    },
    [seTypes.UPDATE]: function ({ x, y, id }) {
      if (!store.state.user.players.uids[id]) {
        store.commit(mTypes.SOCKET_NEW_PLAYER, id)
      }
      id = store.state.user.players.uids[id];
      console.log(seTypes.UPDATE, { x, y, id: id });
      store.commit(mTypes.SOCKET_UPDATE, { x, y, id })
    },
    [seTypes.GAME_OVER]: function (id) {
      console.log(seTypes.GAME_OVER, id);
      store.commit(mTypes.SOCKET_GAME_OVER, id)
    }
  },
  watch: {
    isGameOver: (isOver) => {
      console.log('watch', isOver);
    }
  },
  computed: {
    ...mapState({
      id: (state) => state.user.player.id,
      isGameOver: (state) => state.user.gameOver.isGameOver,
      field: (state) => state.field.field
    })
  },
  methods: {
    goUp () {
      console.log('goUp')
      const x = store.state.user.player.offsetX,
        y = store.state.user.player.offsetY;
      this.$socket.emit(seTypes.GET_FIELD, { x, y: y + 10 });
      store.commit(mTypes.UPDATE_OFFSET, { x, y: y + 10 });
    },
    goDown () {
      console.log('goDown')
      const x = store.state.user.player.offsetX,
        y = store.state.user.player.offsetY;
      this.$socket.emit(seTypes.GET_FIELD, { x, y: y - 10 });
      store.commit(mTypes.UPDATE_OFFSET, { x, y: y - 10 });
    },
    goRight () {
      console.log('goRight')
      const x = store.state.user.player.offsetX,
        y = store.state.user.player.offsetY;
      this.$socket.emit(seTypes.GET_FIELD, { x: x + 10, y });
      store.commit(mTypes.UPDATE_OFFSET, { x: x + 10, y });
    },
    goLeft () {
      console.log('goLeft')
      const x = store.state.user.player.offsetX,
        y = store.state.user.player.offsetY;
      this.$socket.emit(seTypes.GET_FIELD, { x: x - 10, y });
      store.commit(mTypes.UPDATE_OFFSET, { x: x - 10, y });
    }
  }
}
