import { UPDATE, GET_FIELD, NEXT_STEP, GAME_OVER, NEW_PLAYER } from '../socket-event-types'

export const SOCKET_UPDATE = `SOCKET_${UPDATE}`;
export const SOCKET_GET_FIELD = `SOCKET_${GET_FIELD}`;

export { CONNECTION as CONNECT } from '../socket-event-types';
export const END_TURN = 'END_TURN';
export const SOCKET_NEXT_STEP = `SOCKET_${NEXT_STEP}`;
export const SOCKET_GAME_OVER = `SOCKET_${GAME_OVER}`;
export const SOCKET_NEW_PLAYER = `SOCKET_${NEW_PLAYER}`;
export const UPDATE_OFFSET = 'UPDATE_OFFSET';
