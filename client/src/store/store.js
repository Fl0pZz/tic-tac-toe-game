import Vue from 'vue'
import Vuex from 'vuex'
import field from './modules/field/field'
import user from './modules/user/user'
import * as actions from './actions'

Vue.use(Vuex);

const store = new Vuex.Store({
  modules: {
    field,
    user
  },
  actions
});
console.log('aaa');
export default store;
