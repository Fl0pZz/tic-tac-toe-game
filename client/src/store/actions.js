import * as types from './mutations-types'
import { UPDATE } from '../socket-event-types'
import socket from '../ws'

export const update = ({ commit, state }, coords) => {
  console.log('action update', socket);
  commit(types.END_TURN);
  // commit(types.SOCKET_UPDATE, { ...coords, id: state.user.player.id });
  socket.emit(UPDATE, coords)
};
