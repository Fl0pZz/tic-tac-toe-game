import mutations from './user-mutations'

const state = {
  connect: false,
  player: {
    id: null,
    canAct: false,
    offsetX: 0,
    offsetY: 0
  },
  players: {
    id: 1,
    uids: {}
  },
  gameOver: {
    isGameOver: false,
    winner: null
  }
};

const user = {
  state,
  mutations
};

export default user;
