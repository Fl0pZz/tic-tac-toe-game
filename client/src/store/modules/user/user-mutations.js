import * as types from '../../mutations-types'

export default {
  [types.CONNECT]: (state, uid) => {
    state.connect = true;
    state.player.id = uid;
  },
  [types.SOCKET_NEXT_STEP]: (state, id) => {
    if (state.player.id === id) {
      state.player.canAct = true;
    }
  },
  [types.END_TURN]: (state) => {
    state.player.canAct = false;
  },
  [types.SOCKET_GAME_OVER]: (state, id) => {
    state.gameOver = {
      isGameOver: true,
      winner: id
    };
  },
  [types.SOCKET_NEW_PLAYER]: (state, uid) => {
    state.players.uids[uid] = state.players.id++;
  },
  [types.UPDATE_OFFSET]: (state, { x, y }) => {
    state.player.offsetX = x;
    state.player.offsetY = y;
  }
}
