import * as types from '../../mutations-types'

export default {
  [types.SOCKET_GET_FIELD]: (state, field) => {
    for (let y = 0; y < 10; ++y) {
      for (let x = 0; x < 10; ++x) {
        state.field[y][x].owner = field[y][x].owner
      }
    }
    // state.field = field
  },
  [types.SOCKET_UPDATE]: (state, { x, y, id }) => {
    state.field[y][x].owner = id;
  }
}
