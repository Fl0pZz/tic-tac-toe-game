import mutations from './field-mutations'

const createField = () => {
  const field = {};
  for (let y = 0; y < 10; ++y) {
    field[y] = {};
    for (let x = 0; x < 10; ++x) {
      field[y][x] = { owner: null };
    }
  }
  return field
};

const f = createField();

const state = { field: f };

const field = {
  state,
  mutations
};

export default field;
