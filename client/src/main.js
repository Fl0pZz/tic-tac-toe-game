import Vue from 'vue'
import App from './App.vue'
import VueSocketio from 'vue-socket.io'
import socket from './ws'
import store from './store/store'

Vue.use(VueSocketio, socket);

new Vue({
  el: '#app',
  store,
  render: h => h(App)
});
