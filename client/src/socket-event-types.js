export const CONNECTION = 'connect';
export const NEW_PLAYER = 'NEW_PLAYER';
export const PLAYER_LEFT = 'PLAYER_LEFT';
export const UPDATE = 'UPDATE';
export const NEXT_STEP = 'NEXT_STEP';
export const GET_FIELD = 'GET_FIELD';
export const GAME_OVER = 'GAME_OVER';
