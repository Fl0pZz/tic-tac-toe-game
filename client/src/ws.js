import IO from 'socket.io-client'

const socket = IO('http://127.0.0.1:3000');

export default socket;
